//
//  Synth.m
//  Swift Synth
//
//  Created by Djuro on 11/20/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Synth.h"

@interface Synth()
@property (strong, nonatomic) AVAudioEngine *audioEngine;
@property (strong, nonatomic) AVAudioSourceNode *sourceNode;
@property (strong, nonatomic) Signal signal;
@property (nonatomic) double sampleRate;
@property (nonatomic) float deltaTime;
@end

@implementation Synth

#pragma mark - Singleton

+ (instancetype)shared {
    static Synth *shared = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        shared = [[Synth alloc] init];
    });
    
    return shared;
}

#pragma mark - Initializers

- (instancetype)init {
    return [self init:Oscillator.sine];
}

- (instancetype)init:(Signal)signal {
    if (self = [super init]) {
        self.audioEngine = [[AVAudioEngine alloc] init];
        
        AVAudioMixerNode *mainMixer = self.audioEngine.mainMixerNode;
        AVAudioOutputNode *outputNode = self.audioEngine.outputNode;
        AVAudioFormat *format = [outputNode inputFormatForBus:0];
        
        self.sampleRate = format.sampleRate;
        self.deltaTime = 1 / (float)self.sampleRate;
        
        self.signal = signal;
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat sampleRate:format.sampleRate channels:1 interleaved:format.isInterleaved];
        
        [self.audioEngine attachNode:self.sourceNode];
        [self.audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
        [self.audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;
        
        NSError *error;
        [self.audioEngine startAndReturnError:&error];
        
        if (error) {
            NSLog(@"Could not start audioEngine: %@", error.localizedDescription);
        }
    }
    
    return self;
}

#pragma mark - Utilities

- (void)setVolume:(float)volume {
    self.audioEngine.mainMixerNode.outputVolume = volume;
    
    [self.delegate audioEngineIsPlaying:volume > 0.0];
}

- (float)volume {
    return self.audioEngine.mainMixerNode.outputVolume;
}

- (AVAudioSourceNode *)sourceNode {
    if (_sourceNode == nil) {
        __weak __typeof(self) weakSelf = self;
        
        _sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:
                       ^OSStatus(BOOL * _Nonnull isSilence,
                                 const AudioTimeStamp * _Nonnull timestamp,
                                 AVAudioFrameCount frameCount,
                                 AudioBufferList * _Nonnull outputData) {
            __typeof(weakSelf) strongSelf = weakSelf;
            
            if (strongSelf == nil) { return 1; }
            
            for (AVAudioFrameCount i = 0; i < frameCount; i++) {
                float sampleVal = strongSelf.signal(strongSelf.time);
                strongSelf.time += strongSelf.deltaTime;
                  
                for (int j = 0; j < outputData->mNumberBuffers; j++) {
                    float *data = outputData->mBuffers[j].mData;
                    data[i] = sampleVal;
                }
            }
                          
            return noErr;
        }];
    }
    return _sourceNode;
}

#pragma mark - Public API

- (void)setWaveformTo:(Signal)signal {
    self.signal = signal;
}

@end
