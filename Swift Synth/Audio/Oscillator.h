//
//  Oscillator.h
//  Swift Synth
//
//  Created by Djuro on 11/20/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef float (^Signal)(float);

typedef NS_ENUM(NSInteger, Waveform) {
    sine,
    triangle,
    sawtooth,
    square,
    whiteNoise
};

NS_ASSUME_NONNULL_BEGIN

@interface Oscillator : NSObject

+ (float)amplitude;
+ (void)setAmplitude:(float)amplitude;
+ (float)frequency;
+ (void)setFrequency:(float)frequency;

@property (class, nonatomic, readonly) float (^sine)(float t);
@property (class, nonatomic, readonly) float (^triangle)(float t);
@property (class, nonatomic, readonly) float (^sawtooth)(float t);
@property (class, nonatomic, readonly) float (^square)(float t);
@property (class, nonatomic, readonly) float (^whiteNoise)(float t);

@end

NS_ASSUME_NONNULL_END
