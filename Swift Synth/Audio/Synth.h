//
//  Synth.h
//  Swift Synth
//
//  Created by Djuro on 11/20/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Oscillator.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SynthDelegate <NSObject>
- (void)audioEngineIsPlaying:(BOOL)isPlaying;
@end

@interface Synth : NSObject

@property (nonatomic) float volume;
@property (nonatomic) float time;
@property (weak, nonatomic) id<SynthDelegate> delegate;

+ (instancetype)shared;

- (instancetype)init:(Signal)signal;

- (void)setWaveformTo:(Signal)signal;

@end

NS_ASSUME_NONNULL_END
