//
//  Oscillator.m
//  Swift Synth
//
//  Created by Djuro on 11/20/20.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import "Oscillator.h"

@interface Oscillator()
@property (nonatomic) float _amplitude;
@property (nonatomic) float _frequency;
@end

@implementation Oscillator

static float AMPLITUDE = 1.0f;
static float FREQUENCY = 440.0f;

#pragma mark - Utilities

+ (float)amplitude {
    return AMPLITUDE;
}

+ (void)setAmplitude:(float)amplitude {
    AMPLITUDE = amplitude;
}

+ (float)frequency {
    return FREQUENCY;
}

+ (void)setFrequency:(float)frequency {
    FREQUENCY = frequency;
}

#pragma mark - Oscillator Waveforms

+ (float (^)(float t))sine {
    return ^ (float t) { return (float)([Oscillator amplitude] * sin(2 * M_PI * [Oscillator frequency] * t)); };
}

+ (float (^)(float t))triangle {
    return ^ (float t) {
        double period = 1.0 / [Oscillator frequency];
        double currentTime = fmod(t, period);
        double value = currentTime / period;
    
        double result = 0.0;
        
        if (value < 0.25) {
            result = value * 4;
        } else if (value < 0.75) {
            result = 2.0 - (value * 4.0);
        } else {
            result = value * 4 - 4.0;
        }
        
        return (float)([Oscillator amplitude] * result);
    };
}

+ (float (^)(float t))sawtooth {
    return ^ (float t) {
        double period = 1.0 / [Oscillator frequency];
        double currentTime = fmod(t, period);
        float value = [Oscillator amplitude] * ((currentTime / period) * 2 - 1.0);
        
        return value;
    };
}

+ (float (^)(float t))square {
    return ^ (float t) {
        double period = 1.0 / (double)([Oscillator frequency]);
        double currentTime = fmod(t, period);
        float value = ((currentTime / period) < 0.5) ? [Oscillator amplitude] : -1.0 * [Oscillator amplitude];
        
        return value;
    };
}

+ (float (^)(float t))whiteNoise {
    return ^ (float t) {
        float value = (((float)arc4random() / 0x100000000) * 2) - 1;
        
        return [Oscillator amplitude] * value;
    };
}

@end
